package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

//import java.awt.*;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        /*StackPane root  = new StackPane();
        Button abc = new Button("-----------");
        abc.setPrefSize(50,50);
        Button cba = new Button("++");
        cba.setPrefSize(10,10);

        abc.setOnAction(new EventHandler<ActionEvent>() {
            int o=100;
            @Override
            public void handle(ActionEvent event) {
                o=o-1;
                System.out.println(o);
            }
        });
        cba.setOnAction(new EventHandler<ActionEvent>() {
            int i=0;
            @Override

            public void handle(ActionEvent event) {
                i=i+1;
                System.out.println(i);
            }
        });
        root.getChildren().addAll(abc,cba);*/


        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }

}
